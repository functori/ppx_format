## ppx_format

Preprocesser for compact string formatting.
```ocaml
let foo = "foo" in
let bar = 42l in
let f : string -> int64 -> string = {f|$foo $ld{bar} $1 $0 $Ld{2}|f} "bowl" in
...
```
