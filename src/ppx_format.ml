open Ppxlib
open Ast_builder.Default

let exp_of_string ~loc x =
  let lexer =
    let acc = Lexing.from_string ~with_positions:false x in
    acc.Lexing.lex_start_p <- loc.loc_start;
    acc.Lexing.lex_curr_p <- loc.loc_end;
    acc in
  Parse.expression lexer

let flag_str = "\\(#\\|-\\|\\+\\| \\|0\\)"
let width_str  = "\\([0-9]+\\)"
let precision_str  = "\\(\\.[0-9]+\\)"

let conversion_type_str =
  let l = [
    "d"; "i"; "u"; "x"; "X"; "o"; "s"; "S"; "c"; "C"; "f"; "F"; "e"; "E"; "g";
    "G"; "h"; "H"; "b"; "ld"; "li"; "ld"; "lu"; "lx"; "lX"; "lo"; "nd"; "ni";
    "nd"; "nu"; "nx"; "nX"; "no"; "Ld"; "Li"; "Ld"; "Lu"; "Lx"; "LX"; "Lo";
    "z"; "Z"] in
  "\\(" ^ String.concat "\\|" l ^ "\\)"

let var_str = "\\([A-Za-z0-9]\\|_\\)*"
let reg = Str.regexp @@ Format.sprintf "\\$\\(\\(%s?%s?%s?%s\\)?{%s}\\|%s\\)"
    flag_str width_str precision_str conversion_type_str var_str var_str

let efun ~loc ?(prefix="x") ~min ~max e =
  let rec aux i =
    if i > max then e
    else
      let var = prefix ^ string_of_int i in
      pexp_fun ~loc Nolabel None (pvar ~loc var) @@
      aux (i+1) in
  aux min

let debug = match Sys.getenv_opt "PPX_FORMAT_DEBUG" with
  | Some "1" | Some "true" -> true
  | _ -> false

let format ~loc ?(kind="f") ?(args=[]) s =
  let acc = ref [] in
  let unknown = ref None in
  let min_numbers = ref None in
  let prefix = "x" in
  let aux ?(z=false) var =
    let zexp e =
      if z then eapply ~loc (evar ~loc "Z.to_string") [e]
      else e in
    match int_of_string_opt var with
    | Some j ->
      min_numbers := (match !min_numbers with None -> Some j | Some j2 -> Some (min j j2));
      begin match List.nth_opt args j with
        | None ->
          acc := !acc @ [ zexp @@ evar ~loc (prefix ^ string_of_int j) ];
          unknown := begin match !unknown with
            | None -> Some (j, j, 1)
            | Some (mi, ma, _) when j >= mi && j<= ma -> !unknown
            | Some (mi, ma, n) -> Some (min mi j, max ma j, n+1)
          end;
        | Some e -> acc := !acc @ [ zexp e ]
      end
    | None ->
      acc := !acc @ [ zexp @@ exp_of_string ~loc var ] in
  let s = Str.global_substitute reg (fun s ->
      let s = Str.matched_string s in
      match String.index_opt s '{' with
      | None ->
        let var = String.sub s 1 (String.length s - 1) in
        aux var;
        "%s"
      | Some i ->
        let var = String.sub s (i+1) (String.length s - i - 2) in
        let conv, z =
          if i = 1 then "%s", false
          else
            let s = String.sub s 1 (i-1) in
            if s = "z" then "%s", true
            else if s = "Z" then "%S", true
            else "%" ^ s, false in
        aux ~z var;
        conv) s in
  match !min_numbers with
  | Some j when j <> 0 ->
    Location.raise_errorf ~loc "$i arguments should start at $0"
  | _ ->
    let format_fun = match kind with
      | "o" -> "Format.printf"
      | "e" -> "Format.eprintf"
      | _ -> "Format.sprintf" in
    let e = match !unknown with
      | None ->
        eapply ~loc (evar ~loc format_fun) (estring ~loc s :: !acc)
      | Some (min, max, n) ->
        if max - min + 1 <> n || min <> List.length args then
          Location.raise_errorf ~loc "Unused $i argument"
        else
          efun ~loc ~min ~max ~prefix @@
          eapply ~loc (evar ~loc format_fun) (estring ~loc s :: !acc) in
    (if debug then Format.printf "%s@." @@ Pprintast.string_of_expression e);
    e

let catch_f = match Sys.getenv_opt "PPX_FORMAT_F" with
  | Some "1" | Some "true" -> true
  | _ -> false

let handle_kind k = k = "f" || k = "o" || k = "e"

let transform =
  object(_self)
    inherit Ast_traverse.map as super
    method! expression e = match e.pexp_desc with
      | Pexp_apply ({pexp_desc=Pexp_constant (Pconst_string (s, loc, Some k)); _}, l)
        when handle_kind k ->
        let args = List.map snd l in
        format ~loc ~args ~kind:k s
      | Pexp_constant (Pconst_string (s, loc, Some k)) when handle_kind k ->
        format ~loc ~kind:k s
      | Pexp_apply ({pexp_desc=Pexp_ident {txt=Lident k; _}; _},
                    (Nolabel, {pexp_desc=Pexp_constant (Pconst_string (s, loc, None)); _}) :: l)
        when catch_f && handle_kind k ->
        let args = List.map snd l in
        format ~loc ~args ~kind:k s
      | _ -> super#expression e
  end

let () =
  Driver.register_transformation "ppx_format" ~impl:transform#structure
